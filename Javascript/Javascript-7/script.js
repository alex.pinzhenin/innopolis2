"use strict";

let form = document.querySelector('form');
let button = document.querySelector('button');

let inputName = document.querySelector('input[name="name"]');
let errorName = document.querySelector('.error-message-name');

inputName.value = localStorage.getItem('name'); // new

form.addEventListener('submit', validate);
inputName.addEventListener('focus', clearErrorName);
inputName.addEventListener('input', changeName); // new

function validate(event) {
    event.preventDefault();

    let nameLength = inputName.value.trim().length;
    if (nameLength < 3) {
        errorName.classList.add('error');
        errorName.innerHTML = 'Error';
        return;
    }

    // валидация пройдена
    form.reset();
    localStorage.removeItem('name'); // new
    console.log('OK');
}

function clearErrorName() {
    errorName.classList.remove('error');
    errorName.innerHTML = '';
}

function changeName() { // new
    localStorage.setItem('name', inputName.value);
}
