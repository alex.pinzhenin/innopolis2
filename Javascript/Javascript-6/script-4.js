"use strict";

const list = document.getElementsByClassName('list')[0];
console.log(list);

const items = document.getElementsByClassName('item');
console.log(items);

// console.log('for(i)');
for(let i=0; i<items.length; i++) {
    setTimeout(
        () => items[i].style.backgroundColor = 'red',
        1000 * i
    );
}

// console.log('for(in)');
// for(let index in items) {
//     console.log(index);
//     setTimeout(
//         () => items[index].style.backgroundColor = 'red',
//         1000 * index
//     );
// }

// console.log('for(of)');
// for(let item of items) {
//     console.log(item);
// }
