"use strict";

let parent = document.querySelector('.box');
parent.addEventListener('click', () => console.log('clicked'));

let element = document.querySelector('.box > div');
element.addEventListener('click', () => element.innerHTML = 'Hello, Frotenders!');


console.log(element);

// let elements = document.querySelectorAll('.box > div');
// console.log(elements);

// setTimeout(() => element.innerHTML = 'Hello, Frotenders!', 3000 );

// ul > li:last-child

// <ul>
//     <li>...</li>
//     <li>...</li>
//     <li>...</li> <=
// </ul>

// 0) div => все теги <div>

// 1) div.list => все теги <div class="list"> 

// 2) div .list => элемент с классом list, у которого есть предок <div>

// 3) div > .list => элемент с классом list, у которого есть родитель <div>

// 4) .list > div => элемент с тегом <div>, у которого есть родитель с классом list
