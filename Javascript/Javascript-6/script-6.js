"use strict";

let form = document.querySelector('form');
let button = document.querySelector('button');

let inputName = document.querySelector('input[name="name"]');
let errorName = document.querySelector('.error-message-name');

let inputAge = document.querySelector('input[name="age"]');
let errorAge = document.querySelector('.error-message-age');

form.addEventListener('submit', validate);
inputName.addEventListener('focus', clearErrorName);
inputAge.addEventListener('focus', clearErrorAge);

function validate(event) {
    event.preventDefault();

    let nameLength = inputName.value.trim().length;
    if (nameLength >= 3) {
        console.log('OK');
    } else {
        errorName.classList.add('error');
        errorName.innerHTML = 'Error';
        return;
    }

    let age = +inputAge.value;
    if (inputAge.value === '') {
        errorAge.classList.add('error');
        errorAge.innerHTML = 'Error-1';
        return;
    } else if (isNaN(age)) {
        errorAge.classList.add('error');
        errorAge.innerHTML = 'Error-2';
        return;
    } else {
        console.log('OK');
    }
}

function clearErrorName() {
    errorName.classList.remove('error');
    errorName.innerHTML = '';
}

function clearErrorAge() {
    errorAge.classList.remove('error');
    errorAge.innerHTML = '';
}
