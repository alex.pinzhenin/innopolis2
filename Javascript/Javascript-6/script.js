"use strict";

const start = new Date().getTime();

fetch('https://yandex.ru')
    .finally(() => {
        const end = new Date().getTime();
        console.log(`SecondWay: ${end - start}ms`);
    });

