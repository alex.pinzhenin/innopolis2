"use strict";

let objects = [
    {
        key1: 'value1.1',
        key2: 'value1.2',
        key21: 'value1.21',
        key3: 'value1.3',
    },
    {
        key1: 'value2.1',
        key2: 'value2.2',
        key21: 'value1.22',
        key3: 'value2.3',
    },
    {
        key1: 'value3.1',
        key2: 'value3.2',
        key21: 'value1.22',
        key3: 'value3.3',
    }
];

for(let i=0; i<objects.length; i++) {
    console.log(`- ${objects[i].key1} (${objects[i].key3})`);
}

for(let i=0; i<objects.length; i++) {
    let array = Object.values(objects[i]);
    console.log(`- ${array[0]} (${array[2]})`);
}
