"use strict";

let numbers = {
    a0: -1,
    a1: 1,
    a2: 2,
    a3: 3
};

function pow(q) {
    let numbers2 = {};
    for (let key in numbers) {
        numbers2[key] = numbers[key] ** q;
    }
    return numbers2;
}

console.log(pow(2));
console.log(pow(3));
console.log(pow(4));
