"use strict";

// let animal = {
//     type: 'кошка',
//     weight: 5.7, // кг
//     color: 'white',
// };

let type = 'кошка';
let weight = 5.7;
let color = 'white';

let animal1 = { type, weight, color };

let animal2 = { 
    type: type,
    weight: weight, 
    color: color
};

animal1.color = 'black';
animal1.age = 10;

for(let key in animal1) {
    console.log(key, animal1[key]);
}

// console.log(animal1, animal2);

// let animals = { animal1, animal2 };
// console.log(animals);













// let condition = 2;

// // while(condition < 10) {
// //   console.log(condition);
// //   condition = condition + 2;
// // }

// // for(let i=0; i<=20; i+=2) {
// //   console.log(i);
// // }

// function dayOfWeek(dayNumber) {
//     let dayName;
//     if (dayNumber === 1) {
//         dayName = 'понедельник';
//     } else if (dayNumber === 2) {
//         dayName = 'вторник';
//     } else if (dayNumber === 3) {
//         dayName = 'среда';
//     } else if (dayNumber === 4) {
//         dayName = 'четверг';
//     } else if (dayNumber === 5) {
//         dayName = 'пятница';
//     } else if (dayNumber === 6) {
//         dayName = 'суббота';
//     } else if (dayNumber === 7) {
//         dayName = 'воскресенье';
//     }
//     return dayName;
// }

// // function dayOfWeek(dayNumber) {
// //     let dayName;
// //     switch (dayNumber) {
// //         case 1:
// //             dayName = 'понедельник';
// //             break;
// //         case 2:
// //             dayName = 'вторник';
// //             break;
// //         case 3:
// //             dayName = 'среда';
// //             break;
// //         case 4:
// //             dayName = 'четверг';
// //             break;
// //         case 5:
// //             dayName = 'пятница';
// //             break;
// //         case 6:
// //             dayName = 'суббота';
// //             break;
// //         case 7:
// //             dayName = 'воскресенье';
// //             break;
// //     }
// //     return dayName;
// // }

// // function dayOfWeek(dayNumber) {
// //     switch (dayNumber) {
// //         case 1: return 'понедельник';
// //         case 2: return 'вторник';
// //         case 3: return 'среда';
// //         case 4: return 'четверг';
// //         case 5: return 'пятница';
// //         case 6: return 'суббота';
// //         case 7: return 'воскресенье';
// //         default: return null;
// //     }
// // }

// function dayOfWeek(dayNumber) {
//     let days = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];
//     console.log(days[dayNumber - 1]);
// }

// // function dayOfWeek(dayNumber) {
// //     return ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'][dayNumber-1];
// // }

// // for (let i = 1; i <= 7; i++) {
// //     if (i === 6) {
// //         continue;
// //     }
// //     console.log(dayOfWeek(i));
// // }


// console.log('Finish');
