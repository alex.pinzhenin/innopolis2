"use strict";

let good = {
    param: {
        memory: 512,
        color: 'pink',
    },
    reviews: [
        {
            name: '...',
            rating: 5,
            plus: '...',
            minus: '...',
            description: '...'
        },
        {
            name: '...',
            rating: 50,
            plus: '...',
            minus: '...',
            description: '...'
        }
    ],
};

let reviews = good.reviews;
for (let i = 0; i < reviews.length; i++) {
    console.log(reviews[i]);
}
