"use strict";

/**
 * Комментарий, что делает функция
 * @param {Object} obj Проверяемый объект 
 * @returns {boolean}
 */
function isEmpty(obj) {
    for (let key in obj) {
        return false;
    }
    return true;
}

function isEmpty2(obj) {
    return Object.keys(obj).length === 0;
}
