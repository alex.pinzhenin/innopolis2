"use strict";

let numbers = [3, 56, 78, 2, 102, 15];

let g100Count = 0;
numbers.forEach((value) => g100Count += Number(value > 100));
console.log(g100Count);

g100Count = 0;
numbers.forEach(function (value) {
    let greatThan100;
    if (value > 100) {
        greatThan100 = true;
    } else {
        greatThan100 = false;
    }

    if (greatThan100) {
        g100Count++;
    }
});
console.log(g100Count);

g100Count = 0;
for (let i = 0; i < numbers.length; i++) {
    let value = numbers[i];
    let greatThan100;
    if (value > 100) {
        greatThan100 = true;
    } else {
        greatThan100 = false;
    }

    if (greatThan100) {
        g100Count++;
    }
}
console.log(g100Count);

// forEach, map => [], filter => [], some, every


// function g100(value) {
//     return value > 100;
// }

// let g100 = (value) => value > 100;



// Вернёт true, если ХОТЯ БЫ ОДИН элемент больше 100
// let sum = 0;
// let flag = numbers.forEach(function (n) {
//     sum += n;
// });




























