"use strict";

let m1 = [1, 3, 5, 7];

let m2 = [4, 5, 1, 8];

function add(id) {
    if (!m1.includes(id)) {
        m1.push(id);
    }
}

add(m2[0]);
add(m2[1]);
add(m2[2]);
add(m2[3]);

console.log(m1);
