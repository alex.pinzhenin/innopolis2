"use strict";

/** SPLICE */

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let arrDel = arr.splice(5); // <= главное
console.log({ arr, arrDel });

let arr1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let arr1Del = arr1.splice(5, 2); // <= главное
console.log({ arr1, arr1Del });

let arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let arr2Del = arr2.splice(5, 2, 11, 12, 13); // <= главное
console.log({ arr2, arr2Del });
