"use strict";

let count = 5;
let intervalId = setInterval(() => {
    count = count - 1;
    console.log(count);
    if (count === 0) {
        clearInterval(intervalId);
        console.log('Время вышло...');
    }
}, 1000);

console.log('Что-нибудь', intervalId);
