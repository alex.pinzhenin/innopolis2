"use strict";

// console.log('Start');

// let promise = new Promise(function (resolve, reject) {
//     setTimeout(function () { resolve("OK"); }, 3000);
// });
// promise.then((result) => console.log(result));

// console.log('Finish');

function getSupport(obj) {
    obj.data.forEach((user) => console.log(user.id, user.name, user.avatar));
    // console.log(obj.support);
}

console.log('Start');

fetch("https://reqres.in/api/users")
    .then((response) => response.json())
    .then((result) => getSupport(result))
    .catch((error) => console.debug({ error }));

console.log('Finish');
