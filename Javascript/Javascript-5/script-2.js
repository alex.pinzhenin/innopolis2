"use strict";

/** forEach & map */

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let abc = {};
let arrResult = arr.forEach((value, index) => abc['key' + index] = value); // abc.key0 = 1; abc.key1 = 2; ...
console.log({ arr, abc });

let def = arr.reduce((acc, value, index) => ({...acc, ['key'+index]: value}), {});
console.log({def});

// let arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// let arr2Result = arr2.map((value) => value + 1);
// console.log({ arr2, arr2Result });
