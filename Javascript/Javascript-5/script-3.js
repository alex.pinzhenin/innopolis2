"use strict";

/** reduce */

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

let sum = 0;
for (let index = 0; index < arr.length; index++) {
    let value = arr[index]
    sum += value;
}
console.log(sum);

let sum1 = 0;
arr.forEach((value) => sum1 += value);
console.log(sum1);

let sum2 = arr.reduce((sum, value) => sum + value, 0);
console.log(sum2);

let result1 = arr.reduce((acc, value) => acc || value === 5, false);
let result2 = arr.some((value) => value === 5);

// let result = arr.reduce((index, value) => abc['key' + index] = value); // abc.key0 = 1; abc.key1 = 2; ...
// console.log({ arr, abc });
