"use strict";

let countSelector = document.querySelector('.count');
let buttonSelector = document.querySelector('.button');
let goodCount = +localStorage.getItem('goodCount');
render();

buttonSelector.addEventListener('click', toggleGood);

function toggleGood() {
    goodCount = 1 - goodCount;
    localStorage.setItem('goodCount', goodCount);
    render();
}

function render() {
    if (goodCount) {
        countSelector.innerHTML = goodCount;
        countSelector.classList.remove('hidden');
        buttonSelector.classList.remove('zero');
    } else {
        countSelector.classList.add('hidden');
        buttonSelector.classList.add('zero');
    }
} 
