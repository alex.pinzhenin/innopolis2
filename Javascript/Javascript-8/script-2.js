"use strict";

class FormBase {
    finish() {
        alert('Форма отправлена');
    }
}

class Form extends FormBase {
    form;
    name;
    grade;
    text;
    nameError;

    constructor(
        formSelector,
        nameSelector,
        gradeSelector,
        textSelector,
        nameErrorSelector
    ) {
        super();
        this.form = document.querySelector(formSelector);
        this.name = document.querySelector(nameSelector);
        this.grade = document.querySelector(gradeSelector);
        this.text = document.querySelector(textSelector);
        this.nameError = document.querySelector(nameErrorSelector);

        this.form.addEventListener('submit', this.validate.bind(this));
        this.name.addEventListener('focus', this.clearErrorName.bind(this));
    }

    validate(event) {
        event.preventDefault();

        let nameLength = this.name.value.trim().length;
        if (nameLength < 3) {
            this.fillErrorName();
            return;
        }

        // валидация пройдена
        this.form.reset();
        this.finish();
    }

    fillErrorName() {
        this.nameError.classList.add('error');
        this.nameError.innerHTML = 'Error';
    }

    clearErrorName() {
        this.nameError.classList.remove('error');
        this.nameError.innerHTML = '';
    }
}

let review = new Form(
    'form',
    'input[name="name"]',
    'input[name="grade"]',
    'textarea[name="text"]',
    '.error-message-name'
);
