"use strict";

class User {
    _name = "Аноним";

    constructor(name) {
        if (name) {
            this._name = name;
        }
    }

    get name() {
        return this._name;        
    }

    set name(value) {
        if(this.validateName(value)) {
            alert(`Ошибка: ${value}`);
            return;
        }
        this._name = value;
        console.log(`Успех: ${value}`);
    }

    validateName(value) {
        return value.trim().length < 2;
    }
}

let noname = new User();
let user1 = new User('Алексей');

user1.name = '';
user1.name = 'a';
user1.name = 'ab';
user1.name = 'abc';
user1.name = ' a ';
user1.name = 'bla-bla-bla';
