import { createSlice } from '@reduxjs/toolkit';

const LOCAL_STORAGE_KEY = 'initialStateCounter';

export const saveToLocalStorage = (store) => (dispatch) => (action) => {
    console.log("saveToLocalStorage", action, store.getState());
    dispatch(action);
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(store.getState().counter));
};

export const counterSlice = createSlice({
    name: 'counter', // Имя слайса

    initialState: JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || { left: 0, right: 0 },

    reducers: { // Все доступные методы
        addLeft: (prevState, action) => {
            const left = prevState.left + Number(action.payload);
            localStorage.setItem('left', left);
            return {
                left,
                right: prevState.right
            }
        },
        addRight: (prevState, action) => ({
            left: prevState.left,
            right: prevState.right + Number(action.payload.number)
        }),
        setRight: (prevState, action) => ({
            left: prevState.left,
            right: Number(action.payload)
        }),
    }
});

// Экспортируем наружу все действия
export const { addLeft, addRight, setRight } = counterSlice.actions;

export default counterSlice.reducer;
