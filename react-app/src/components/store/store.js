import { configureStore } from '@reduxjs/toolkit';
import counterReducer, { saveToLocalStorage } from './counter-reducer';

const logger = (store) => (dispatch) => (action) => {
    console.log("logger", action);
    dispatch(action);
    // console.log("next state", store.getState());
};

export const store = configureStore({
    reducer: {
        counter: counterReducer,
    },
    // middleware: [saveToLocalStorage, logger]
});
