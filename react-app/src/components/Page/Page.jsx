import GrandParent from './GrandParent';
import MemoInput from './MemoInput';
import './page.css';

function Page() {
  return (
    <>
      <GrandParent />
      <MemoInput />
    </>
  );
}

export default Page;
