import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addLeft, addRight, setRight } from '../store/counter-reducer';
import './page.css';

function Child1() {
  const counter = useSelector((store) => store.counter);
  const [number, setNumber] = useState(0);
  const dispatch = useDispatch();

  const handleClickLeft = () => {
    const action = addLeft(number);
    dispatch(action);
  };
  const handleClickRight = () => dispatch(addRight({ number }));
  const handleChangeInput = (event) => setNumber(event.target.value);

  useEffect(
    () => {
      const initialRight = Number(localStorage.getItem('right')) || 0;
      dispatch(setRight(initialRight));
    },
    []
  );

  useEffect(
    () => {
      counter.right
        ? localStorage.setItem('right', counter.right)
        : localStorage.removeItem('right');
    },
    [counter.right]
  );

  return (
    <div>
      Child-1<br />
      <input type="number" onChange={handleChangeInput} value={number} />
      <button onClick={handleClickLeft}>{counter.left}{number >= 0 ? '+' : ''}{number}</button>
      <button onClick={handleClickRight}>{counter.right}{number >= 0 ? '+' : ''}{number}</button>
    </div>
  );
}

export default Child1;
