import { useState, useMemo } from 'react';

const validateName = (value) => {
  console.log('validateName', value);
  const newValue = value.trim();
  if (!newValue) {
    return 'Введите имя';
  }
  if (newValue.length < 3) {
    return 'Имя должно содержать 3+ букв';
  }
  return null;
};

const validateGrade = (value) => {
  console.log('validateGrade', value);
  const newValue = value.trim();
  if (newValue.length === 0) {
    return 'Укажите оценку';
  }
  if (Number(newValue) < 1 || Number(newValue) > 5) {
    return 'Введите число от 1 до 5';
  }
  return null;
};

function MemoInput() {
  const [inputName, setInputName] = useState('');
  const [inputGrade, setInputGrade] = useState('');

  const errorName = useMemo(
    () => validateName(inputName),
    [inputName]
  );
  const errorGrade = useMemo(
    () => validateGrade(inputGrade),
    [inputGrade]
  );

  const handleChangeName = (event) => setInputName(event.target.value);
  const handleChangeGrade = (event) => setInputGrade(event.target.value);

  return (
    <form>
      <div>
        <input name="name" value={inputName} onChange={handleChangeName} autoComplete="off" />
        <div className="error">{errorName}</div>
      </div>
      <div>
        <input name="grade" value={inputGrade} onChange={handleChangeGrade} autoComplete="off" />
        <div className="error">{errorGrade}</div>
      </div>
    </form>
  );
}

export default MemoInput;
