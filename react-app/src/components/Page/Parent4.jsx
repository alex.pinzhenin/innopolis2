import Child2 from './Child2';
import './page.css';

function Parent4() {
  return (
    <div>
      Parent-4
      <Child2 />
    </div>
  );
}

export default Parent4;
