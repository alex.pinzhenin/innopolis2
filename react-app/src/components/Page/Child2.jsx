import { useSelector } from 'react-redux';
import './page.css';

function Child2() {
  const counter = useSelector((store) => store.counter);

  return (
    <div>
      Child-2<br />
      <div className="counters">
        <span>{counter.left}</span>
        <span>{counter.right}</span>
      </div>
    </div>
  );
}

export default Child2;
