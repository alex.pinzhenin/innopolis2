import './page.css';
import Parent1 from './Parent1.jsx';
import Parent2 from './Parent2.jsx';
import Parent3 from './Parent3.jsx';
import Parent4 from './Parent4.jsx';

function GrandParent() {
  return (
    <div>
      GrandParent
      <div className="parents">
        <Parent1 />
        <Parent2 />
        <Parent3 />
        <Parent4 />
      </div>
    </div>
  );
}

export default GrandParent;
