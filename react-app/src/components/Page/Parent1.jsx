import Child1 from './Child1';
import './page.css';

function Parent1() {
  return (
    <div>
      Parent-1
      <Child1 />
    </div>
  );
}

export default Parent1;
