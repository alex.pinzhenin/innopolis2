import './style.css';

function Link(props) {
  let { text, link, index } = props;

  return (
    <>
      {index ? <span>{'>'}</span> : null}
      <a href={link}>{text}</a>
    </>
  );
}

export default Link;
