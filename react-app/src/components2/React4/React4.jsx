import './main.css';
import reactLogo from './logo192.png';

function React4() {
  const imageSrc = "/images/favicon.ico.webp";
  return (
    <>
      <img src={imageSrc} alt="logo" className="image" />
      <img src={reactLogo} alt="logo" className="image" />
    </>
  );
}

export default React4;
