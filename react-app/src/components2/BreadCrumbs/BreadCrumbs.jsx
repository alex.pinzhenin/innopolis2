import Link from '../Link/Link';
import './style.css';

function BreadCrumbs(props) {
  let { list } = props;

  return (
    <div className="bread-crumbs">
      <div>BreadCrumbs</div>
      {
        list.map(({text, link}, index) => <Link text={text} link={link} index={index} key={text} />)
      }
    </div>
  );

  // let list2 = [];
  // list.forEach((value, index) => {
  //   list2.push(<Link text={value.text} link={value.link} key={value.text} />);
  //   list2.push(<span key={index}>{'>'}</span>);
  // });
  // list2.pop();

  // return (
  //   <div className="bread-crumbs">
  //     <div>BreadCrumbs</div>
  //     {list2}
  //   </div>
  // );
}

export default BreadCrumbs;
