import './header.css';

function Header(props) {
  let { color, backgroundColor, fontSize } = props;

  return (
    <header className="header" style={{color, backgroundColor, fontSize}}>
      This is header
    </header>
  );
}

export default Header;
