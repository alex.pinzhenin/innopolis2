import { useState } from 'react';
import './styles.css';

const errorInitial = { name: null, grade: null, description: null };

function ReviewForm(props) {
  const [input, setInput] = useState(JSON.parse(localStorage.getItem('reviewFormInput')));
  const [error, setError] = useState({ ...errorInitial });
  localStorage.setItem('reviewFormInput', JSON.stringify(input));

  console.log(input);

  const handleChangeName = (event) => { setInput({ ...input, name: event.target.value }) };
  const handleFocusName = (event) => { setError({ ...errorInitial }) };
  const handleChangeGrade = (event) => { setInput({ ...input, grade: Number(event.target.value) }) };
  const handleFocusGrade = (event) => { setError({ ...errorInitial }) };
  const handleChangeDescription = (event) => { setInput({ ...input, description: event.target.value }) };
  const handleFocusDescription = (event) => { setError({ ...errorInitial }) };
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(event);

    const newError = {};

    if (input?.name.length < 2) {
      newError.name = 'Имя короткое';
    }

    if (input?.grade < 1 || input.grade > 5) {
      newError.grade = 'Оценка некорректная';
    }

    setError(newError);

    console.log('Проверка завершена');
  };

  return (
    <form
      className="form"
      onSubmit={handleSubmit}
    >

      <input
        name="name"
        value={input?.name}
        placeholder="Имя Фамилия"
        onChange={handleChangeName}
        onFocus={handleFocusName}
      />
      <div className={`error-message ${error?.name ? '' : 'hidden'}`}>{error?.name}</div>

      <input
        name="grade"
        value={input?.grade}
        placeholder="Оценка"
        onChange={handleChangeGrade}
        onFocus={handleFocusGrade}
      />
      <div className={`error-message ${error?.grade ? '' : 'hidden'}`}>{error?.grade}</div>

      <textarea
        name="description"
        placeholder="Текст отзыва"
        onChange={handleChangeDescription}
        onFocus={handleFocusDescription}
        defaultValue={input?.description}
      />

      <button type="submit">Отправить</button>

    </form>
  );
}

export default ReviewForm;
