import './styles.css';

function ReviewItem(props) {
  const { reviewItem, isActive } = props;

  return (
    <div className={`review ${isActive ? 'active' : ''}`}>
      <div className="avatar"><img className="image" src={reviewItem.avatar} alt="review"/></div>
      <div className="name">{reviewItem.name}</div>
      <div>{reviewItem.description}</div>
    </div>
  );
}

export default ReviewItem;
