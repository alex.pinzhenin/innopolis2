import { useState } from 'react';
import ReviewItem from './ReviewItem';
import ReviewForm from './ReviewForm';
import './styles.css';

function ReviewList(props) {
  const { reviewList } = props;
  const [idActive, setIdActive] = useState(null);

  const handleClick = (id) => setIdActive(id);

  // function handleClick(id) {
  //   setIdActive(id);
  // }

  return (
    <div className="reviews">
      <div>Reviews</div>
      {
        reviewList.map((reviewItem) => (
          <div onClick={() => handleClick(reviewItem.id)} key={reviewItem.id}>
            <ReviewItem
              reviewItem={reviewItem}
              isActive={reviewItem.id === idActive}
            />
          </div>
        ))
      }
      <div>
        <ReviewForm />
      </div>
    </div>
  );
}

export default ReviewList;
