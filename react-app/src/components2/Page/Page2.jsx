import Menu from './Menu';
import './page.css';

function Page2() {
  return (
    <>
      <Menu />
      <div>page-2</div>
    </>
  );
}

export default Page2;
