import { useEffect, useState } from 'react';
import Menu from './Menu';
import './page.css';

function Effect() {
  const [click, setClick] = useState(0);
  const [backgroundClass, setBackgroundClass] = useState('background-red');

  useEffect(() => {
    console.log(click);
    if(click > 5) {
      setTimeout(() => setBackgroundClass('background-blue'), 3000);
    }
  }, [click]);

  return (
    <>
      <Menu />
      <div className={backgroundClass} onClick={() => setClick(click + 1)}>

      </div>
    </>
  );
}

export default Effect;
