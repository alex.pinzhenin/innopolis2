import Menu from './Menu';
import './page.css';

function Page1() {
  return (
    <>
      <Menu />
      <div>page-1</div>
    </>
  );
}

export default Page1;
