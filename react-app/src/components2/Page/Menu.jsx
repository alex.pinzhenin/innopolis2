import { Link } from 'react-router-dom';
import './page.css';

function Menu() {
  return (
    <div className="nav">
      <Link to="/" className="new-class">Индекс</Link>
      <Link to="/page1">Стр.1</Link>
      <Link to="/page2">Стр.2</Link>
      <Link to="/page3">Стр.3</Link>
    </div>
  );
}

export default Menu;
