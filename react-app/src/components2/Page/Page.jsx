import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Index from './Index';
import Page1 from './Page1';
import Page2 from './Page2';
import Page3 from './Page3';
import Effect from './Effect';
import './page.css';

function Page() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Index />} />
          <Route path="/page1" element={<Page1 />} />
          <Route path="/page2" element={<Page2 />} />
          <Route path="/page3" element={<Page3 />} />
          <Route path="/effect" element={<Effect />} />
          {/* <Route path="*" element={<PageNotFound />} /> */}
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default Page;
