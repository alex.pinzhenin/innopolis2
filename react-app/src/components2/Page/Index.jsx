import Menu from './Menu';
import './page.css';

function Index() {
  return (
    <>
      <Menu />
      <div>Index page</div>
    </>
  );
}

export default Index;
