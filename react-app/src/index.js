import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { store } from './components/store/store';
import Page from './components/Page/Page';

const root = createRoot(document.getElementById('root'));

root.render(
  <Provider store={store}>
    <Page />
  </Provider>
);
